Расписание поездок курьеров
===========================

Установка
---------
```bash
$ cp .env.dist .env
$ docker-compose up --build -d
$ docker-compose exec app bash -c "composer install"
$ docker-compose exec app bash -c "./bin/doctrine migrations:migrate"
$ docker-compose exec app bash -c "./bin/doctrine fixtures:load"
```