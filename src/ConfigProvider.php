<?php

declare(strict_types=1);

namespace Schedule;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Psr\Container\ContainerInterface;
use Schedule\Application\Handler\ArrivalHandler;
use Schedule\Application\Handler\FormHandler;
use Schedule\Application\Handler\HomePageHandler;
use Schedule\Application\Service\TravelService;
use Schedule\Infrastructure\Persistence\DoctrineCourierRepository;
use Schedule\Infrastructure\Persistence\DoctrineRegionRepository;
use Schedule\Model\Courier;
use Schedule\Model\CourierRepositoryInterface;
use Schedule\Model\Region;
use Schedule\Model\RegionRepositoryInterface;
use Schedule\Model\Travel;
use Schedule\Model\TravelRepositoryInterface;
use Schedule\Model\TravelServiceInterface;

/**
 * The configuration provider for the App module
 *
 * @see https://docs.zendframework.com/zend-component-installer/
 */
class ConfigProvider
{
    /**
     * Returns the configuration array
     *
     * To add a bit of a structure, each section is defined in a separate
     * method which returns an array with its configuration.
     *
     */
    public function __invoke(): array
    {
        return [
            'dependencies' => $this->getDependencies(),
            'templates' => $this->getTemplates(),
        ];
    }

    /**
     * Returns the container dependencies
     */
    public function getDependencies(): array
    {
        return [
            'invokables' => [

            ],
            'factories' => [
                ArrivalHandler::class => [ArrivalHandler::class, 'create'],
                HomePageHandler::class => [HomePageHandler::class, 'create'],
                FormHandler::class => [FormHandler::class, 'create'],
                TravelServiceInterface::class => function (ContainerInterface $container) {
                    /** @var TravelRepositoryInterface $travelRepository */
                    $travelRepository = $container->get(TravelRepositoryInterface::class);

                    return new TravelService($travelRepository);
                },
                CourierRepositoryInterface::class => function (ContainerInterface $container) {
                    /** @var EntityManagerInterface $em */
                    $em = $container->get(EntityManagerInterface::class);
                    /** @var EntityRepository $repository */
                    $repository = $em->getRepository(Courier::class);

                    return new DoctrineCourierRepository($repository);
                },
                RegionRepositoryInterface::class => function (ContainerInterface $container) {
                    /** @var EntityManagerInterface $em */
                    $em = $container->get(EntityManagerInterface::class);
                    /** @var EntityRepository $repository */
                    $repository = $em->getRepository(Region::class);

                    return new DoctrineRegionRepository($repository);
                },
                TravelRepositoryInterface::class => function (ContainerInterface $container) {
                    /** @var EntityManagerInterface $em */
                    $em = $container->get(EntityManagerInterface::class);
                    /** @var EntityRepository $repository */
                    return $em->getRepository(Travel::class);
                },
            ],
        ];
    }

    /**
     * Returns the templates configuration
     */
    public function getTemplates(): array
    {
        return [
            'paths' => [
                'application' => ['templates/application'],
                'error' => ['templates/error'],
                'layout' => ['templates/layout'],
            ],
        ];
    }
}
