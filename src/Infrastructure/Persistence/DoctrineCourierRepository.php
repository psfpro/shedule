<?php


namespace Schedule\Infrastructure\Persistence;


use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\EntityRepository;
use Schedule\Infrastructure\Doctrine\QueryBuilderCollection;
use Schedule\Model\Courier;
use Schedule\Model\CourierRepositoryInterface;

class DoctrineCourierRepository implements CourierRepositoryInterface
{
    /**
     * @var EntityRepository
     */
    private $entityRepository;

    public function __construct(EntityRepository $entityRepository)
    {

        $this->entityRepository = $entityRepository;
    }

    public function findAll(): Collection
    {
        $qb = $this->entityRepository->createQueryBuilder('courier');

        return new QueryBuilderCollection($qb);
    }

    /**
     * @param int $id
     * @return null|Courier
     */
    public function findOneById(int $id): ?Courier
    {
        return $this->entityRepository->find($id);
    }

    /**'
     * @param string $string
     * @return Collection
     */
    public function findByFullName(string $string): Collection
    {
        // TODO: Implement findByFullName() method.
    }
}