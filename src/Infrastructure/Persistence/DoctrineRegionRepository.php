<?php


namespace Schedule\Infrastructure\Persistence;


use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\EntityRepository;
use Schedule\Infrastructure\Doctrine\QueryBuilderCollection;
use Schedule\Model\Region;
use Schedule\Model\RegionRepositoryInterface;

class DoctrineRegionRepository implements RegionRepositoryInterface
{
    /**
     * @var EntityRepository
     */
    private $entityRepository;

    public function __construct(EntityRepository $entityRepository)
    {

        $this->entityRepository = $entityRepository;
    }

    public function findAll(): Collection
    {
        $qb = $this->entityRepository->createQueryBuilder('region');

        return new QueryBuilderCollection($qb);
    }

    public function findOneById(int $id): ?Region
    {
        /** @var Region|null $region */
        $region = $this->entityRepository->find($id);

        return $region;
    }
}