<?php


namespace Schedule\Infrastructure\Persistence;


use DateTimeImmutable;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\EntityRepository;
use Schedule\Infrastructure\Doctrine\QueryBuilderCollection;
use Schedule\Model\Courier;
use Schedule\Model\Travel;
use Schedule\Model\TravelRepositoryInterface;

class DoctrineTravelRepository extends EntityRepository implements TravelRepositoryInterface
{
    public function findAll(): Collection
    {
        $qb = $this->createQueryBuilder('travel');
        $qb->orderBy('travel.departureDate', Criteria::DESC);

        return new QueryBuilderCollection($qb);
    }

    /**
     * @param Courier $courier
     * @param DateTimeImmutable $start
     * @param DateTimeImmutable $end
     * @return null|Travel
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findOneByPeriod(Courier $courier, DateTimeImmutable $start, DateTimeImmutable $end): ?Travel
    {
        $qb = $this->createQueryBuilder('travel');
        $qb->where('NOT (travel.departureDate >= :end OR travel.arrivalDate <= :start)')
            ->andWhere('travel.courier = :courier')
        ->setParameters([
            'courier' => $courier,
            'start' => $start,
            'end' => $end,
        ])->setMaxResults(1);

        return $qb->getQuery()->getOneOrNullResult();
    }

    /**
     * @param Travel $travel
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function save(Travel $travel): void
    {
        $this->getEntityManager()->persist($travel);
        $this->getEntityManager()->flush();
    }
}