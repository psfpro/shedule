<?php


namespace Schedule\Infrastructure\Extractor;


interface ExtractorInterface
{
    /**
     * @param object $object
     * @return array
     */
    public function extract($object);
}