<?php


namespace Schedule\Infrastructure\Fixtures;


use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Schedule\Model\Region;

class RegionFixture implements FixtureInterface
{

    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $regions = [
            'Санкт-Петербург',
            'Уфа',
            'Нижний Новгород',
            'Владимир',
            'Кострома',
            'Екатеринбург',
            'Ковров',
            'Воронеж',
            'Самара',
            'Астрахань'
        ];

        foreach ($regions as $name) {
            $region = new Region($name, rand(1, 10));
            $manager->persist($region);
        }

        $manager->flush();
    }
}