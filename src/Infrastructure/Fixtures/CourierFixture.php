<?php


namespace Schedule\Infrastructure\Fixtures;


use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Faker\Factory;
use Schedule\Model\Courier;

class CourierFixture implements FixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $faker = Factory::create('Ru_RU');
        for ($i=0; $i < 100; $i++) {
            $name = $faker->name;
            $courier = new Courier($name);
            $manager->persist($courier);
        }

        $manager->flush();
    }
}