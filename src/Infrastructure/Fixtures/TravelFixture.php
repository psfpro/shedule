<?php


namespace Schedule\Infrastructure\Fixtures;


use DateInterval;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Schedule\Model\Courier;
use Schedule\Model\Region;
use Schedule\Model\Travel;

class TravelFixture implements FixtureInterface
{
    private $startDate;
    private $endDate;
    private $regions;
    private $regionsCount;

    /**
     * TravelFixture constructor.
     * @throws \Exception
     */
    public function __construct()
    {

    }

    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     * @throws \Exception
     */
    public function load(ObjectManager $manager)
    {
        $courierRepository = $manager->getRepository(Courier::class);
        $couriers = $courierRepository->findAll();

        $regionRepository = $manager->getRepository(Region::class);

        $this->startDate = new \DateTimeImmutable('2015-06-01');
        $this->endDate = new \DateTimeImmutable();
        $this->regions = $regionRepository->findAll();
        $this->regionsCount = count($this->regions);

        foreach ($couriers as $courier) {
            $this->createTravels($manager, $courier, $this->startDate);
            $manager->flush();
        }
    }

    /**
     * @param ObjectManager $manager
     * @param Courier $courier
     * @param \DateTimeImmutable $date
     * @throws \Exception
     */
    protected function createTravels(ObjectManager $manager, Courier $courier, \DateTimeImmutable $date)
    {
        $region = $this->regions[rand(1, $this->regionsCount - 1)];
        $travel = new Travel($courier, $region, $date);
        $manager->persist($travel);
        if ($travel->getArrivalDate() < $this->endDate) {
            $nextDate = $travel->getArrivalDate()->add(new DateInterval('P' . rand(1, 3) . 'D'));
            $this->createTravels($manager, $courier, $nextDate);
        }
    }
}