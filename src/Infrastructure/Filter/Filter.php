<?php


namespace Schedule\Infrastructure\Filter;


class Filter
{
    public static function variable($variable, ?int $filter = FILTER_DEFAULT, $options = null)
    {
        $result = filter_var($variable, $filter, $options);

        if ($result === false && $filter !== FILTER_VALIDATE_BOOLEAN) {
            return null;
        } else {
            if (is_array($result) && in_array($options, [FILTER_REQUIRE_ARRAY, FILTER_FORCE_ARRAY])) {
                foreach ($result as $key => $value) {
                    if ($value === false && $filter !== FILTER_VALIDATE_BOOLEAN) {
                        unset($result[$key]);
                    }
                }
            }

            return $result;
        }
    }

    public static function post(string $variable, ?int $filter = FILTER_DEFAULT, $options = null)
    {
        $result = filter_input(INPUT_POST, $variable, $filter, $options);

        if ($result === false && $filter !== FILTER_VALIDATE_BOOLEAN) {
            return null;
        } else {
            if (is_array($result) && in_array($options, [FILTER_REQUIRE_ARRAY, FILTER_FORCE_ARRAY])) {
                foreach ($result as $key => $value) {
                    if ($value === false && $filter !== FILTER_VALIDATE_BOOLEAN) {
                        unset($result[$key]);
                    }
                }
            }

            return $result;
        }
    }

    public static function get(string $variable, ?int $filter = FILTER_DEFAULT, $options = null)
    {
        $result = filter_input(INPUT_GET, $variable, $filter, $options);

        if ($result === false && $filter !== FILTER_VALIDATE_BOOLEAN) {
            return null;
        } else {
            if (is_array($result) && in_array($options, [FILTER_REQUIRE_ARRAY, FILTER_FORCE_ARRAY])) {
                foreach ($result as $key => $value) {
                    if ($value === false && $filter !== FILTER_VALIDATE_BOOLEAN) {
                        unset($result[$key]);
                    }
                }
            }

            return $result;
        }
    }
}