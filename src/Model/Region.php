<?php


namespace Schedule\Model;


class Region
{
    /**
     * @var int
     */
    private $id;
    /**
     * @var string
     */
    private $name;
    /**
     * @var int
     */
    private $travelDuration;

    /**
     * Region constructor.
     * @param string $name
     * @param int $travelDuration
     */
    public function __construct(string $name, int $travelDuration)
    {
        $this->name = $name;
        $this->travelDuration = $travelDuration;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return int
     */
    public function getTravelDuration(): int
    {
        return $this->travelDuration;
    }

    /**
     * @param \DateTimeImmutable $departureDate
     * @return \DateTimeImmutable
     * @throws \Exception
     */
    public function calculateArrivalDate(\DateTimeImmutable $departureDate)
    {
        return $departureDate->add(new \DateInterval('P' . $this->getTravelDuration() . 'D'));
    }
}