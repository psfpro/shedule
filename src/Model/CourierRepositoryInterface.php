<?php


namespace Schedule\Model;


use Doctrine\Common\Collections\Collection;

interface CourierRepositoryInterface
{
    /**
     * @return Collection
     */
    public function findAll(): Collection;

    /**
     * @param int $id
     * @return null|Courier
     */
    public function findOneById(int $id): ?Courier;

    /**'
     * @param string $string
     * @return Collection
     */
    public function findByFullName(string $string): Collection;
}