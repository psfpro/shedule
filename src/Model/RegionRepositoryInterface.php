<?php


namespace Schedule\Model;


use Doctrine\Common\Collections\Collection;

interface RegionRepositoryInterface
{
    public function findAll(): Collection;

    public function findOneById(int $id): ?Region;
}