<?php


namespace Schedule\Model;


use DateTimeImmutable;

interface TravelServiceInterface
{
    /**
     * @param Courier $courier
     * @param Region $region
     * @param DateTimeImmutable $departureDate
     * @return Travel
     */
    public function create(Courier $courier, Region $region, DateTimeImmutable $departureDate): Travel;
}