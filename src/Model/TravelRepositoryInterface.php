<?php


namespace Schedule\Model;


use DateTimeImmutable;
use Doctrine\Common\Collections\Collection;

interface TravelRepositoryInterface
{
    /**
     * @return Collection
     */
    public function findAll(): Collection;

    /**
     * @param Courier $courier
     * @param DateTimeImmutable $start
     * @param DateTimeImmutable $end
     * @return Travel
     */
    public function findOneByPeriod(Courier $courier, DateTimeImmutable $start, DateTimeImmutable $end): ?Travel;

    /**
     * @param Travel $travel
     */
    public function save(Travel $travel): void;
}