<?php


namespace Schedule\Model;


use DateInterval;
use DateTimeImmutable;

class Travel
{
    /**
     * @var int
     */
    private $id;
    /**
     * @var Courier
     */
    private $courier;
    /**
     * @var Region
     */
    private $region;
    /**
     * @var \DateTimeImmutable
     */
    private $departureDate;
    /**
     * @var \DateTimeImmutable
     */
    private $arrivalDate;

    /**
     * Travel constructor.
     * @param Courier $courier
     * @param Region $region
     * @param DateTimeImmutable $departureDate
     * @throws \Exception
     */
    public function __construct(Courier $courier, Region $region, DateTimeImmutable $departureDate)
    {
        $this->courier = $courier;
        $this->region = $region;
        $this->departureDate = $departureDate;
        $this->arrivalDate = $departureDate->add(new DateInterval('P' . $region->getTravelDuration() . 'D'));
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return Courier
     */
    public function getCourier(): Courier
    {
        return $this->courier;
    }

    /**
     * @return Region
     */
    public function getRegion(): Region
    {
        return $this->region;
    }

    /**
     * @return \DateTimeImmutable
     */
    public function getDepartureDate(): \DateTimeImmutable
    {
        return $this->departureDate;
    }

    /**
     * @return \DateTimeImmutable
     */
    public function getArrivalDate(): \DateTimeImmutable
    {
        return $this->arrivalDate;
    }
}