<?php


namespace Schedule\Application\Validator;


use DateInterval;
use Schedule\Model\Courier;
use Schedule\Model\CourierRepositoryInterface;
use Schedule\Model\Region;
use Schedule\Model\RegionRepositoryInterface;
use Schedule\Model\TravelRepositoryInterface;

class CreateTravelValidator
{
    /**
     * @var Courier
     */
    private $courier;
    /**
     * @var Region
     */
    private $region;
    /**
     * @var \DateTimeImmutable
     */
    private $departureDate;
    /**
     * @var CourierRepositoryInterface
     */
    private $courierRepository;
    /**
     * @var RegionRepositoryInterface
     */
    private $regionRepository;
    /**
     * @var TravelRepositoryInterface
     */
    private $travelRepository;

    /**
     * CreateTravelValidator constructor.
     * @param CourierRepositoryInterface $courierRepository
     * @param RegionRepositoryInterface $regionRepository
     * @param TravelRepositoryInterface $travelRepository
     */
    public function __construct(
        CourierRepositoryInterface $courierRepository,
        RegionRepositoryInterface $regionRepository,
        TravelRepositoryInterface $travelRepository
    )
    {
        $this->courierRepository = $courierRepository;
        $this->regionRepository = $regionRepository;
        $this->travelRepository = $travelRepository;
    }

    /**
     * @param $data
     * @return array
     * @throws \Exception
     */
    public function validate($data)
    {
        $errors = [];
        
        $regionId = isset($data['region']) ? (int) $data['region'] : null;
        if (empty($regionId)) {
            $errors['region'] = 'Необходимо заполнить Регион';
        } else {
            $region = $this->regionRepository->findOneById($regionId);
            if (empty($region)) {
                $errors['region'] = 'Регион не найден (ID: ' . $regionId . ')';
            } else {
                $this->region = $region;
            }
        }

        $courierId = isset($data['courier']) ? (int) $data['courier'] : null;
        if (empty($courierId)) {
            $errors['courier'] = 'Необходимо заполнить Курьер';
        } else {
            $courier = $this->courierRepository->findOneById($courierId);
            if (empty($courier)) {
                $errors['courier'] = 'Курьер не найден (ID: ' . $courierId . ')';
            } else {
                $this->courier = $courier;
            }
        }

        $departureDate = isset($data['departureDate']) ? $data['departureDate'] : null;
        if (empty($departureDate)) {
            $errors['departureDate'] = 'Необходимо заполнить Дату отправления';
        } else {
            $this->departureDate = new \DateTimeImmutable($departureDate);
        }

        if (empty($this->region) || empty($this->courier) || empty($this->departureDate)) {
            $errors['arrivalDate'] = 'Невозможно рассчитать дату прибытия';
        } else {
            $arrivalDate = $this->departureDate->add(new DateInterval('P' . $this->region->getTravelDuration() . 'D'));
            $travel = $this->travelRepository->findOneByPeriod($this->courier, $this->departureDate, $arrivalDate);
            if ($travel) {
                $errors['arrivalDate'] = 'Пересечение с поездкой в регион ' . $travel->getRegion()->getName()
                    . ' с ' . $travel->getDepartureDate()->format('d.m.Y')
                    . ' по ' . $travel->getArrivalDate()->format('d.m.Y');
            }
        }
        
        return $errors;
    }

    /**
     * @return Courier
     */
    public function getCourier(): Courier
    {
        return $this->courier;
    }

    /**
     * @return Region
     */
    public function getRegion(): Region
    {
        return $this->region;
    }

    /**
     * @return \DateTimeImmutable
     */
    public function getDepartureDate(): \DateTimeImmutable
    {
        return $this->departureDate;
    }
}