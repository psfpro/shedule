<?php


namespace Schedule\Application\Handler;


use Psr\Container\ContainerInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Schedule\Application\Service\TravelService;
use Schedule\Application\Validator\CreateTravelValidator;
use Schedule\Model\CourierRepositoryInterface;
use Schedule\Model\RegionRepositoryInterface;
use Schedule\Model\TravelRepositoryInterface;
use Schedule\Model\TravelServiceInterface;
use Symfony\Component\Validator\ConstraintViolation;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Zend\Diactoros\Response\HtmlResponse;
use Zend\Diactoros\Response\JsonResponse;
use Zend\Expressive\Template\TemplateRendererInterface;

class FormHandler implements RequestHandlerInterface
{
    /**
     * @var CourierRepositoryInterface
     */
    private $courierRepository;
    /**
     * @var RegionRepositoryInterface
     */
    private $regionRepository;
    /**
     * @var TemplateRendererInterface
     */
    private $templateRenderer;
    /**
     * @var TravelRepositoryInterface
     */
    private $travelRepository;
    /**
     * @var TravelServiceInterface
     */
    private $travelService;

    /**
     * HomePageHandler constructor.
     * @param CourierRepositoryInterface $courierRepository
     * @param RegionRepositoryInterface $regionRepository
     * @param TravelRepositoryInterface $travelRepository
     * @param TemplateRendererInterface $templateRenderer
     * @param TravelServiceInterface $travelService
     */
    private function __construct(
        CourierRepositoryInterface $courierRepository,
        RegionRepositoryInterface $regionRepository,
        TravelRepositoryInterface $travelRepository,
        TemplateRendererInterface $templateRenderer,
        TravelServiceInterface $travelService
    )
    {
        $this->courierRepository = $courierRepository;
        $this->regionRepository = $regionRepository;
        $this->travelRepository = $travelRepository;
        $this->templateRenderer = $templateRenderer;
        $this->travelService = $travelService;
    }

    static public function create(ContainerInterface $container)
    {
        return new static(
            $container->get(CourierRepositoryInterface::class),
            $container->get(RegionRepositoryInterface::class),
            $container->get(TravelRepositoryInterface::class),
            $container->get(TemplateRendererInterface::class),
            $container->get(TravelServiceInterface::class)
        );
    }

    /**
     * Handles a request and produces a response.
     *
     * May call other collaborating code to generate the response.
     * @param ServerRequestInterface $request
     * @return ResponseInterface
     * @throws \Exception
     */
    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        if ($request->getMethod() == 'POST') {
            $validator = new CreateTravelValidator(
                $this->courierRepository,
                $this->regionRepository,
                $this->travelRepository
            );
            $errors = $validator->validate($request->getParsedBody());

            if (count($errors) != 0) {

                return new JsonResponse($errors, 422);
            } else {
                $travel =$this->travelService->create(
                    $validator->getCourier(),
                    $validator->getRegion(),
                    $validator->getDepartureDate()
                );

                return new JsonResponse([
                    'id' => $travel->getId(),
                    'region' => $travel->getRegion()->getName(),
                    'departure_date' => $travel->getDepartureDate()->format('Y.m.d'),
                    'arrival_date' => $travel->getArrivalDate()->format('Y.m.d'),
                ]);
            }

        }
        $couriers = $this->courierRepository->findAll();
        $regions = $this->regionRepository->findAll();
        return new HtmlResponse($this->templateRenderer->render('application::form', [
            'couriers' => $couriers,
            'regions' => $regions,
        ]));
    }
}