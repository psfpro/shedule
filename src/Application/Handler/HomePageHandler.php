<?php


namespace Schedule\Application\Handler;


use Doctrine\Common\Collections\Criteria;
use Doctrine\Common\Collections\Selectable;
use Psr\Container\ContainerInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Schedule\Infrastructure\Doctrine\DataProvider;
use Schedule\Model\TravelRepositoryInterface;
use Zend\Diactoros\Response\HtmlResponse;
use Zend\Expressive\Template\TemplateRendererInterface;

class HomePageHandler implements RequestHandlerInterface
{
    /**
     * @var TemplateRendererInterface
     */
    private $templateRenderer;
    /**
     * @var TravelRepositoryInterface
     */
    private $travelRepository;

    /**
     * HomePageHandler constructor.
     * @param TravelRepositoryInterface $travelRepository
     * @param TemplateRendererInterface $templateRenderer
     */
    private function __construct(
        TravelRepositoryInterface $travelRepository,
        TemplateRendererInterface $templateRenderer
    )
    {
        $this->templateRenderer = $templateRenderer;
        $this->travelRepository = $travelRepository;
    }

    static public function create(ContainerInterface $container)
    {
        return new static(
            $container->get(TravelRepositoryInterface::class),
            $container->get(TemplateRendererInterface::class)
        );
    }

    /**
     * Handles a request and produces a response.
     *
     * May call other collaborating code to generate the response.
     * @param ServerRequestInterface $request
     * @return ResponseInterface
     */
    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $travels = $this->travelRepository->findAll();
        if ($travels instanceof Selectable) {
            $criteria = Criteria::create();
            if ($dateFrom = isset($request->getQueryParams()['dateFrom']) ? $request->getQueryParams()['dateFrom'] : null) {
                $criteria->andWhere(Criteria::expr()->gt('departureDate', new \DateTime($dateFrom)));
            }
            if ($dateTo = isset($request->getQueryParams()['dateTo']) ? $request->getQueryParams()['dateTo'] : null) {
                $criteria->andWhere(Criteria::expr()->lt('departureDate', new \DateTime($dateTo)));
            }
            $travels = $travels->matching($criteria);
        }


        $dataProvider = new DataProvider($travels);

        return new HtmlResponse($this->templateRenderer->render('application::home-page', compact([
            'dataProvider', 'dateFrom', 'dateTo'
        ])));
    }
}