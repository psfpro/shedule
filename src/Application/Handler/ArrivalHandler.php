<?php


namespace Schedule\Application\Handler;


use Psr\Container\ContainerInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Schedule\Application\Service\TravelService;
use Schedule\Application\Validator\CreateTravelValidator;
use Schedule\Model\CourierRepositoryInterface;
use Schedule\Model\RegionRepositoryInterface;
use Schedule\Model\TravelRepositoryInterface;
use Schedule\Model\TravelServiceInterface;
use Symfony\Component\Validator\ConstraintViolation;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Zend\Diactoros\Response\EmptyResponse;
use Zend\Diactoros\Response\HtmlResponse;
use Zend\Diactoros\Response\JsonResponse;
use Zend\Expressive\Template\TemplateRendererInterface;

class ArrivalHandler implements RequestHandlerInterface
{
    /**
     * @var RegionRepositoryInterface
     */
    private $regionRepository;

    /**
     * ArrivalHandler constructor.
     * @param RegionRepositoryInterface $regionRepository
     */
    private function __construct(
        RegionRepositoryInterface $regionRepository
    )
    {
        $this->regionRepository = $regionRepository;
    }

    static public function create(ContainerInterface $container)
    {
        return new static(
            $container->get(RegionRepositoryInterface::class)
        );
    }

    /**
     * Handles a request and produces a response.
     *
     * May call other collaborating code to generate the response.
     * @param ServerRequestInterface $request
     * @return ResponseInterface
     * @throws \Exception
     */
    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        if ($request->getMethod() == 'POST') {
            $data = $request->getParsedBody();
            $regionId = isset($data['region']) ? $data['region'] : null;
            if (!empty($regionId)) {
                $region = $this->regionRepository->findOneById($regionId);
            }
            $departureDate = isset($data['departureDate']) ? new \DateTimeImmutable($data['departureDate']) : null;
            if (empty($region) || empty($departureDate)) {
                return new JsonResponse('Невозможно расчитать дату прибытия', 422);
            } else {
                return new JsonResponse($region->calculateArrivalDate($departureDate)->format('d.m.Y'));
            }

        }

        return new EmptyResponse();
    }
}