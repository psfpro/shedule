<?php


namespace Schedule\Application\Service;


use DateTimeImmutable;
use Schedule\Model\Courier;
use Schedule\Model\Region;
use Schedule\Model\Travel;
use Schedule\Model\TravelRepositoryInterface;
use Schedule\Model\TravelServiceInterface;

class TravelService implements TravelServiceInterface
{
    /**
     * @var TravelRepositoryInterface
     */
    private $travelRepository;

    /**
     * TravelService constructor.
     * @param TravelRepositoryInterface $travelRepository
     */
    public function __construct(TravelRepositoryInterface $travelRepository)
    {
        $this->travelRepository = $travelRepository;
    }

    /**
     * @param Courier $courier
     * @param Region $region
     * @param DateTimeImmutable $departureDate
     * @return Travel
     * @throws \Exception
     */
    public function create(Courier $courier, Region $region, DateTimeImmutable $departureDate): Travel
    {
        $travel = new Travel($courier, $region, $departureDate);
        $this->travelRepository->save($travel);

        return $travel;
    }
}