#!/usr/bin/env php
<?php

require __DIR__.'/../vendor/autoload.php';

use Doctrine\DBAL\Migrations\Configuration\Configuration;
use Doctrine\DBAL\Migrations\Tools\Console\Helper\ConfigurationHelper;
use Doctrine\DBAL\Tools\Console\Helper\ConnectionHelper;
use Doctrine\ORM\Tools\Console\ConsoleRunner;
use Doctrine\ORM\Tools\Console\Helper\EntityManagerHelper;
use Schedule\Infrastructure\Doctrine\FixturesCommand;
use Symfony\Component\Console\Helper\HelperSet;
use Doctrine\DBAL\Migrations\Tools\Console\ConsoleRunner as MigrationsConsoleRunner;
use Symfony\Component\Console\Helper\QuestionHelper;

/** @var \Psr\Container\ContainerInterface $container */
$container = require __DIR__ . '/../config/container.php';
$entityManager = $container->get(\Doctrine\ORM\EntityManagerInterface::class);

$config = new Configuration($entityManager->getConnection());
$config->setMigrationsDirectory(realpath(__DIR__ . '/../src/Infrastructure/Migrations'));
$config->setMigrationsNamespace('Schedule\Infrastructure\DoctrineMigrations');

$console = ConsoleRunner::createApplication(new HelperSet(
    [
        'db' => new ConnectionHelper($entityManager->getConnection()),
        'em' => new EntityManagerHelper($entityManager),
        'configuration' => new ConfigurationHelper($entityManager->getConnection(), $config),
        'question' => new QuestionHelper(),
    ]
));

MigrationsConsoleRunner::addCommands($console);


$console->add(new FixturesCommand($entityManager, realpath(__DIR__ . '/../src/Infrastructure/Fixtures')));

$console->run();
